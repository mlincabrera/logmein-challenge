## cardGameApi ##

This API to create a card game has the following requirements:

- Create and delete a game
- Create a deck
- Add a deck to a game deck
- Add and remove players from a game
- Deal cards to a player in a game from the game deck
- Get the list of cards for a player
- Get the list of players in a game along with the total added value of all the cards each player holds
- Get the count of how many cards per suit are left undealt in the game deck (example: 5 hearts, 3 spades, etc.)
- Get the count of each card (suit and value) remaining in the game deck sorted by suit (hearts, spades, clubs, and
  diamonds) and face value from high value to low value (King, Queen, Jack, 10….2, Ace with value of 1)
- Shuffle the game deck (shoe)

### Architectural description ###

- Gradle is used as build automation tool. build.gradle file has details about dependencies and commands used.
- Spring boot is used as main framework. There are some environment variables to customize the profile and the DB info.
  `SPRING_PROFILES_ACTIVE`: to set the profile.
  `DB_HOST, DB_PORT, DATABASE, DB_USER, DB_PASS`: to set h2 database info.
- Docker is used to building and run the app agnostic to the environment.
- The architecture manages different http status.
  `Status 200`: for valid requests.
  `Status 400`: for invalid requests.
  `Status 500`: for unknowns exception.

### Dependencies ###

To build and run the app you need

1. `Java 11`
2. `Gradle 6.0+` or the gradle wrapper `./gradlew`
3. `Docker` (optional) To build the app agnostic to the environment.

### Run the API ###

- Using gradle: ```gradle bootRun```
- Using gradle wrapper: ```./gradlew bootRun```
- Using docker: <b>Note:</b> Docker image needs to be build [See this section](#build-docker-image)
  1.```docker run -it --name logmein_challenge -p 8080:8080 -e DB_HOST=host.docker.internal  docker.io/mlincabrera/logmein-challenge:1.0.0-SNAPSHOT --net=bridge```
- Using IntelliJ IDEA IDE <b>Note:</b> Most be active Annotation processor, and the lombok plugin installed

### Run unit test ###

Most run gradle command ```./gradlew test```

### Run integration test ###

Most run gradle command ```./gradlew integrationTest```

### Run code coverage report on console ###

To show code coverage of unit test most run ```./gradlew jacocoLogTestCoverage```

### Analysis of static code with sonarqube ###

Most run command ```./gradlew sonarqube``` or download sonar scanner
from [Sonar Scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/) and run ```sonar-scanner```

### Security analysis with OWASP ###

To identifies known vulnerable dependencies used by the project, most run ```./gradlew dependencyCheckAnalyze```

### Build docker image ###

Must run command (docker is mandatory)
```docker build -t docker.io/mlincabrera/logmein-challenge:1.0.0-SNAPSHOT .```
or
```./gradlew bootBuildImage --imageName=mlincabrera/logmein-challenge:1.0.0-SNAPSHOT```

### Open Api specification ###

http://localhost:8080/cardGameApi/v1/v3/api-docs

### Swagger UI ###

http://localhost:8080/cardGameApi/v1/swagger-ui.html

### Metrics ###

Metrics is expose in prometheus format

http://localhost:8080/cardGameApi/v1/actuator/prometheus

### Quality code ###

[![Quality gate](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=alert_status)](https://sonarcloud.io/dashboard?id=mlincabrera_logmein-challenge)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=coverage)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=coverage&view=list)

[![Technical deb](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=sqale_index)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=sqale_index&view=list)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=vulnerabilities)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=vulnerabilities&view=list)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=bugs)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=bugs&view=list)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=code_smells)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=code_smells&view=list)

[![ncloc](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=ncloc)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=ncloc&view=list)

[![duplicated_lines_density](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=duplicated_lines_density)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=duplicated_lines_density&view=list)

[![Security rating](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=security_rating)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=security_rating&view=list)

[![Reliability rating](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=reliability_rating)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=reliability_rating&view=list)

[![Sqale rating](https://sonarcloud.io/api/project_badges/measure?project=mlincabrera_logmein-challenge&metric=sqale_rating)](https://sonarcloud.io/component_measures?id=mlincabrera_logmein-challenge&metric=sqale_rating&view=list)







