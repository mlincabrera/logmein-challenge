package com.logmein.challenge;

import com.logmein.challenge.service.DeckService;
import com.logmein.challenge.service.GameService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CardGameApplicationTests {

    @Autowired
    private transient GameService gameService;
    @Autowired
    private transient DeckService deckService;

    @Test
    public void testApplicationStarts() {
        CardGameApplication.main("--spring.main.web-environment=true");

        assertNotNull(gameService);
        assertNotNull(deckService);
    }


}
