package com.logmein.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import com.logmein.challenge.CardGameApplication;
import com.logmein.challenge.model.SuitType;
import com.logmein.challenge.service.IDeckService;
import com.logmein.challenge.service.IGameService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = CardGameApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@AutoConfigureMockMvc
public class GameControllerIntegrationTest {

    public static final String CONTENT_TYPE = "application/json";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private IGameService gameService;

    @Autowired
    private IDeckService deckService;

    private Faker faker;

    @BeforeEach
    public void onSetUp() {
        faker = new Faker();
    }

    @Test
    public void createGame() throws Exception {
        var name = faker.name().name();
        mockMvc.perform(post("/games")
                        .contentType(CONTENT_TYPE)
                        .content(name))
                .andExpect(status().isOk());
    }

    @Test
    public void deleteGame() throws Exception {
        var gameId = gameService.createGame(faker.name().name());
        mockMvc.perform(delete("/games/" + gameId)
                        .contentType(CONTENT_TYPE))
                .andExpect(status().isNoContent());
    }

    @Test
    public void addDeckToGame() throws Exception {
        var gameId = gameService.createGame(faker.name().name());
        var deck = deckService.createDeck();
        mockMvc.perform(post("/games/" + gameId + "/deck/" + deck.getId())
                        .contentType(CONTENT_TYPE))
                .andExpect(status().isNoContent());
    }

    @Test
    public void addPlayerToGame() throws Exception {
        var gameId = gameService.createGame(faker.name().name());
        var name = faker.name().name();
        mockMvc.perform(post("/games/" + gameId + "/players/")
                        .contentType(CONTENT_TYPE)
                        .content(name))
                .andExpect(status().isOk());
    }

    @Test
    public void dealCards() throws Exception {
        var name = faker.name().name();
        var amountOfCards = faker.number().randomDigitNotZero();
        var gameId = gameService.createGame(faker.name().name());
        var playerId = gameService.addPlayerToGame(gameId, name);
        mockMvc.perform(post("/games/" + gameId + "/players/" + playerId + "/deal/" + amountOfCards)
                        .contentType(CONTENT_TYPE))
                .andExpect(status().isNoContent());


    }

    @Test
    public void listOfCardsForaPlayer() throws Exception {
        var cards = faker.number().randomDigit();
        var name = faker.name().name();
        var gameId = gameService.createGame(faker.name().name());
        var deckId = deckService.createDeck().getId();
        gameService.addDeckToGame(gameId, deckId);
        var playerId = gameService.addPlayerToGame(gameId, name);
        gameService.dealCards(gameId, cards, playerId);
        mockMvc.perform(get("/games/" + gameId + "/players/" + playerId + "/listOfCardsForaPlayer")
                        .contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(cards)));
    }

    @Test
    public void getTheListOfPlayers() throws Exception {
        var name = faker.name().name();
        var gameId = gameService.createGame(faker.name().name());
        gameService.addPlayerToGame(gameId, name);
        gameService.addPlayerToGame(gameId, name);

        mockMvc.perform(get("/games/" + gameId + "/players")
                        .contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(2)));
    }

    @Test
    public void countOfHowManyCardsPerSuitType() throws Exception {
        var cards = faker.number().randomDigit();
        var name = faker.name().name();
        var gameId = gameService.createGame(faker.name().name());
        var playerId = gameService.addPlayerToGame(gameId, name);
        var deckId = deckService.createDeck().getId();
        gameService.addDeckToGame(gameId, deckId);
        gameService.dealCards(gameId, cards, playerId);

        mockMvc.perform(get("/games/" + gameId + "/countOfHowManyCardsPerSuitType")
                        .contentType(CONTENT_TYPE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.length()", is(SuitType.values().length)));
    }

    @Test
    public void countOfHowManyCardsPerSuitAndFaceType() throws Exception {
        var cards = faker.number().randomDigit();
        var name = faker.name().name();
        var gameId = gameService.createGame(faker.name().name());
        var playerId = gameService.addPlayerToGame(gameId, name);
        var deckId = deckService.createDeck().getId();
        gameService.addDeckToGame(gameId, deckId);
        gameService.dealCards(gameId, cards, playerId);

        mockMvc.perform(get("/games/" + gameId + "/countOfHowManyCardsPerSuitAndFaceType")
                        .contentType(CONTENT_TYPE))
                .andExpect(status().isOk());
    }

    @Test
    public void shuffle() throws Exception {
        var cards = faker.number().randomDigit();
        var name = faker.name().name();
        var gameId = gameService.createGame(faker.name().name());
        var playerId = gameService.addPlayerToGame(gameId, name);
        var deckId = deckService.createDeck().getId();
        gameService.addDeckToGame(gameId, deckId);
        gameService.dealCards(gameId, cards, playerId);

        mockMvc.perform(post("/games/" + gameId + "/shuffle")
                        .contentType(CONTENT_TYPE))
                .andExpect(status().isNoContent());
    }


}
