package com.logmein.challenge.controller;


import com.logmein.challenge.model.Deck;
import com.logmein.challenge.service.DeckService;
import lombok.Getter;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@Getter
@ExtendWith(SpringExtension.class)
class DeckControllerTest {
    @InjectMocks
    private DeckController deckController;

    @Mock
    private DeckService deckService;

    @Test
    void createDeck() {
        when(deckService.createDeck()).thenReturn(Deck.builder().build());

        ResponseEntity<Integer> response = deckController.createDeck();
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}