package com.logmein.challenge.controller;

import com.github.javafaker.Faker;
import com.logmein.challenge.dto.CardDTO;
import com.logmein.challenge.dto.TotalAddedValuePlayerDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitAndFaceTypeDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitTypeDTO;
import com.logmein.challenge.service.GameService;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


@Getter
@ExtendWith(SpringExtension.class)
class GameControllerTest {
    @InjectMocks
    private GameController gameController;
    private Faker faker;

    @Mock
    private GameService gameService;

    @BeforeEach
    public void onSetUp() {
        faker = new Faker();
    }

    @Test
    void createGame() {
        String name = faker.name().name();
        Integer expectedId = faker.number().randomDigitNotZero();
        when(gameService.createGame(name)).thenReturn(expectedId);
        ResponseEntity<Integer> response = gameController.createGame(faker.name().name());
        assertEquals(HttpStatus.OK, response.getStatusCode());

    }

    @Test
    void deleteGame() {
        Integer gameId = faker.number().randomDigitNotZero();
        doNothing().when(gameService).deleteGame(gameId);
        ResponseEntity<Void> response = gameController.deleteGame(gameId);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void addDeckToGame() {
        Integer gameId = faker.number().randomDigitNotZero();
        Integer deckId = faker.number().randomDigitNotZero();
        doNothing().when(gameService).addDeckToGame(gameId, deckId);
        ResponseEntity<Void> response = gameController.addDeckToGame(gameId, deckId);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void addPlayerToGame() {
        String name = faker.name().name();
        Integer gameId = faker.number().randomDigitNotZero();
        Integer playerId = faker.number().randomDigitNotZero();
        when(gameService.addPlayerToGame(gameId, name)).thenReturn(playerId);
        ResponseEntity<Integer> response = gameController.addPlayerToGame(gameId, name);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void removePlayerToGame() {
        Integer gameId = faker.number().randomDigitNotZero();
        Integer playerId = faker.number().randomDigitNotZero();
        doNothing().when(gameService).removePlayerToGame(gameId, playerId);
        ResponseEntity<Void> response = gameController.removePlayerToGame(gameId, playerId);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void dealCards() {
        Integer gameId = faker.number().randomDigitNotZero();
        Integer playerId = faker.number().randomDigitNotZero();
        Integer amountOfCards = faker.number().randomDigitNotZero();
        doNothing().when(gameService).dealCards(gameId, amountOfCards, playerId);
        ResponseEntity<Void> response = gameController.dealCards(gameId, amountOfCards, playerId);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }

    @Test
    void listOfCardsForaPlayer() {
        Integer gameId = faker.number().randomDigitNotZero();
        Integer playerId = faker.number().randomDigitNotZero();
        when(gameService.getTheListOfCardsForaPlayer(gameId, playerId)).thenReturn(new ArrayList<>());
        ResponseEntity<List<CardDTO>> response = gameController.listOfCardsForaPlayer(gameId, playerId);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }




    @Test
    void listOfPlayers() {
        Integer gameId = faker.number().randomDigitNotZero();
        when(gameService.getTheListOfPlayers(gameId)).thenReturn(new ArrayList<>());
        ResponseEntity<List<TotalAddedValuePlayerDTO>> response = gameController.listOfPlayers(gameId);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void countOfHowManyCardsPerSuitType() {
        Integer gameId = faker.number().randomDigitNotZero();
        when(gameService.getCountOfHowManyCardsPerSuitType(gameId)).thenReturn(new ArrayList<>());
        ResponseEntity<List<TotalUndealtCardBySuitTypeDTO>> response = gameController.countOfHowManyCardsPerSuitType(gameId);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void countOfHowManyCardsPerSuitAndFaceType() {
        Integer gameId = faker.number().randomDigitNotZero();
        when(gameService.getCountOfHowManyCardsPerSuitAndFaceType(gameId)).thenReturn(new ArrayList<>());
        ResponseEntity<List<TotalUndealtCardBySuitAndFaceTypeDTO>> response = gameController.countOfHowManyCardsPerSuitAndFaceType(gameId);
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void shuffle() {
        Integer gameId = faker.number().randomDigitNotZero();
        doNothing().when(gameService).shuffle(gameId);
        ResponseEntity<Void> response = gameController.shuffle(gameId);
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}