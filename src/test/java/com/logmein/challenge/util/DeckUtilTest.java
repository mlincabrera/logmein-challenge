package com.logmein.challenge.util;

import com.github.javafaker.Faker;
import com.logmein.challenge.model.Card;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DeckUtilTest {

    private Faker faker = new Faker();

    @Test
    void generateCards() {
        Set<Card> cards = DeckUtil.generateCards();

        assertEquals(52, cards.size());
    }

    @Test
    void getIndexIterator() {
        var size = faker.number().randomDigitNotZero();
        Iterator<Integer> indexIterator = DeckUtil.getIndexIterator(size);
        long count = StreamSupport.stream(Spliterators.spliteratorUnknownSize(indexIterator, Spliterator.ORDERED), false).count();

        assertEquals(count, size);
    }
}