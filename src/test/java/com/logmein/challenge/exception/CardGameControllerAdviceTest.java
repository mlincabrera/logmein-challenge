package com.logmein.challenge.exception;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.persistence.EntityNotFoundException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(SpringExtension.class)
class CardGameControllerAdviceTest {

    private final Faker faker = new Faker();
    private CardGameControllerAdvice cardGameControllerAdvice = new CardGameControllerAdvice();

    @Test
    void testCardGameExceptionHandler() {
        ErrorResponse errorResponse = cardGameControllerAdvice.cardGameExceptionHandler(
                new CardGameException(ErrorCode.NOT_FOUND, new EntityNotFoundException()));
        assertNotNull(errorResponse);
        assertEquals(ErrorCode.NOT_FOUND.getCode(), errorResponse.getCode());
        assertEquals(ErrorCode.NOT_FOUND.getMessage(), errorResponse.getMessage());
    }

    @Test
    void testGenericExceptionHandler() {
        var errorMessage = faker.address().fullAddress();
        ErrorResponse errorResponse = cardGameControllerAdvice.genericExceptionHandler(new NullPointerException(errorMessage));
        assertNotNull(errorResponse);
        assertEquals(ErrorCode.GENERIC_ERROR.getCode(), errorResponse.getCode());
        assertEquals(errorMessage, errorResponse.getMessage());
    }
}