package com.logmein.challenge.service;

import com.github.javafaker.Faker;
import com.logmein.challenge.model.Card;
import com.logmein.challenge.model.Deck;
import com.logmein.challenge.model.FaceType;
import com.logmein.challenge.respository.DeckRepository;
import lombok.Getter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;


@Getter
@ExtendWith(SpringExtension.class)
class DeckServiceTest {
    @InjectMocks
    private DeckService deckService;
    @Mock
    private DeckRepository deckRepository;

    private Faker faker;

    @BeforeEach
    public void onSetUp() {
        faker = new Faker();
    }

    @Test
    void save() {
        Deck deck = Deck.builder().build();
        when(deckRepository.save(deck)).thenReturn(deck);
        Deck expectedDeck = deckService.save(deck);
        Assertions.assertEquals(expectedDeck, deck);
    }

    @Test
    void createDeck() {
        Integer expectedDeckId = faker.number().randomDigitNotZero();
        Deck deck = Deck.builder().id(expectedDeckId).build();
        when(deckRepository.save(any())).thenReturn(deck);
        var deckSaved = deckService.createDeck();
        Assertions.assertEquals(52, deckSaved.getCards().size());
    }

    @Test
    void getById() {
        Integer expectedDeckId = faker.number().randomDigitNotZero();
        Deck deck = Deck.builder().id(expectedDeckId).build();
        when(deckRepository.getById(any())).thenReturn(deck);
        Deck expectedDeck = deckService.getById(expectedDeckId);
        Assertions.assertEquals(expectedDeck, deck);
    }

    @Test
    void nextCardToDeal() {
        Integer expectedDeckId = faker.number().randomDigitNotZero();
        Card expectedCard = Card.builder().face(FaceType.EIGHT).id(faker.number().randomDigitNotZero()).build();
        Set<Card> cards = new HashSet<>();
        cards.add(expectedCard);
        Deck deck = Deck.builder().id(expectedDeckId).cards(cards).build();
        Card card = deckService.nextCardToDeal(deck);
        Assertions.assertEquals(card, expectedCard);
    }

    @Test
    void shuffle() {
        Integer expectedDeckId = faker.number().randomDigitNotZero();
        Card expectedCard = Card.builder().face(FaceType.EIGHT).id(faker.number().randomDigitNotZero()).build();
        Set<Card> cards = new HashSet<>();
        cards.add(expectedCard);
        Deck deck = Deck.builder().id(expectedDeckId).cards(cards).build();
        when(deckRepository.save(any())).thenReturn(deck);
        deckService.shuffle(deck);
        verify(deckRepository, times(1)).save(deck);
    }
}