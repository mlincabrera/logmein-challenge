package com.logmein.challenge.service;

import com.github.javafaker.Faker;
import com.logmein.challenge.dto.CardDTO;
import com.logmein.challenge.dto.TotalAddedValuePlayerDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitAndFaceTypeDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitTypeDTO;
import com.logmein.challenge.exception.CardGameException;
import com.logmein.challenge.model.*;
import com.logmein.challenge.respository.GameRepository;
import com.logmein.challenge.respository.PlayerRepository;
import com.logmein.challenge.util.DeckUtil;
import lombok.Getter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Getter
@ExtendWith(SpringExtension.class)
class GameServiceTest {
    @InjectMocks
    private GameService gameService;

    private Faker faker;
    @Mock
    private DeckService deckService;
    @Mock
    private GameRepository gameRepository;
    @Mock
    private PlayerRepository playerRepository;

    @BeforeEach
    public void onSetUp() {
        faker = new Faker();
    }

    @Test
    void createGame() {
        Integer expectedId = faker.number().randomDigitNotZero();
        when(gameRepository.save(any(Game.class))).thenReturn(Game.builder().id(expectedId).build());
        Integer gameId = gameService.createGame(faker.name().name());
        Assertions.assertEquals(expectedId, gameId);
    }

    @Test
    void deleteGame() {
        Integer gameId = faker.number().randomDigitNotZero();
        doNothing().when(gameRepository).deleteById(gameId);
        gameService.deleteGame(gameId);
        verify(gameRepository, times(1)).deleteById(gameId);
    }

    @Test
    void addDeckToGame() {
        Integer gameId = faker.number().randomDigitNotZero();
        Integer deckId = faker.number().randomDigitNotZero();
        when(gameRepository.getById(gameId)).thenReturn(Game.builder().id(gameId).build());
        when(deckService.getById(any())).thenReturn(Deck.builder().id(deckId).build());
        when(gameRepository.save(any(Game.class))).thenReturn(Game.builder().id(gameId).build());
        gameService.addDeckToGame(gameId, deckId);
        verify(gameRepository, times(1)).getById(gameId);
        verify(deckService, times(1)).getById(deckId);
        verify(gameRepository, times(1)).save(any());
    }

    @Test
    void addPlayerToGame() {
        Integer gameId = faker.number().randomDigitNotZero();
        String playerName = faker.name().name();
        Integer expectedPlayerId = faker.number().randomDigitNotZero();
        when(gameRepository.getById(gameId)).thenReturn(Game.builder().id(gameId).build());
        when(playerRepository.save(any(Player.class))).thenReturn(Player.builder().name(playerName).id(expectedPlayerId).build());
        when(gameRepository.save(any(Game.class))).thenReturn(Game.builder().id(gameId).build());
        Integer playerId = gameService.addPlayerToGame(gameId, playerName);
        Assertions.assertEquals(expectedPlayerId, playerId);

    }

    @Test
    void removePlayerToGame() {
        Integer gameId = faker.number().randomDigitNotZero();
        Integer playerId = faker.number().randomDigitNotZero();
        when(gameRepository.getById(gameId)).thenReturn(Game.builder().id(gameId).build());
        when(gameRepository.save(any(Game.class))).thenReturn(Game.builder().id(gameId).build());
        gameService.removePlayerToGame(gameId, playerId);
        verify(gameRepository, times(1)).getById(gameId);
        verify(gameRepository, times(1)).save(any());
    }

    @Test
    void dealCards() {
        Integer gameId = faker.number().randomDigitNotZero();
        Integer playerId = faker.number().randomDigitNotZero();
        Integer amountOfCard = faker.number().randomDigitNotZero();
        Deck deck = Deck.builder().id(faker.number().randomDigitNotZero()).cards(DeckUtil.generateCards()).build();
        Set<Deck> decks = new HashSet<>();
        decks.add(deck);
        when(gameRepository.getById(gameId)).thenReturn(Game.builder().id(gameId).decks(decks).build());
        when(playerRepository.getById(playerId)).thenReturn(Player.builder().id(playerId).build());
        when(deckService.nextCardToDeal(deck)).thenReturn(Card.builder().build());
        when(deckService.save(deck)).thenReturn(deck);
        when(playerRepository.save(any(Player.class))).thenReturn(Player.builder().id(playerId).build());

        gameService.dealCards(gameId, amountOfCard, playerId);

        verify(gameRepository, times(1)).getById(gameId);
        verify(playerRepository).getById(playerId);
        verify(deckService, times(amountOfCard)).nextCardToDeal(any());
        verify(playerRepository, times(amountOfCard)).save(any());
    }

    @Test
    void shuffle() {
        Integer gameId = faker.number().randomDigitNotZero();
        Deck deck = deckService.createDeck();
        Set<Deck> decks = new HashSet<>();
        decks.add(deck);
        when(gameRepository.getById(gameId)).thenReturn(Game.builder().id(gameId).decks(decks).build());
        doNothing().when(deckService).shuffle(deck);
        gameService.shuffle(gameId);
        verify(gameRepository, times(1)).getById(gameId);
        verify(deckService, atLeast(1)).shuffle(deck);
    }

    @Test
    void getTheListOfCardsForaPlayer() {
        Integer playerId = faker.number().randomDigitNotZero();
        Set<Card> cards = new HashSet<>();
        cards.add(Card.builder().face(FaceType.EIGHT).build());
        var gameId = faker.number().randomDigitNotZero();
        when(playerRepository.getById(playerId)).thenReturn(Player.builder().id(playerId).cards(cards)
                .gameId(gameId).build());
        List<CardDTO> cardDTOS = gameService.getTheListOfCardsForaPlayer(gameId, playerId);
        Assertions.assertEquals(cardDTOS.size(), cards.size());
    }

    @Test
    void getTheListOfCardsForaPlayerGameNotFound() {
        Integer playerId = faker.number().randomDigitNotZero();
        Set<Card> cards = new HashSet<>();
        cards.add(Card.builder().face(FaceType.EIGHT).build());
        var gameId = faker.number().randomDigitNotZero();
        when(playerRepository.getById(playerId)).thenReturn(Player.builder().id(playerId).cards(cards)
                .gameId(gameId-1).build());
        assertThrows(CardGameException.class,()->{
            gameService.getTheListOfCardsForaPlayer(gameId, playerId);
        });
    }

    @Test
    void getTheListOfPlayers() {
        Integer gameId = faker.number().randomDigitNotZero();
        String playerName = faker.name().name();
        Set<Card> cards = new HashSet<>();
        cards.add(Card.builder().face(FaceType.EIGHT).build());
        List<Player> players = new ArrayList<>();
        players.add(Player.builder().cards(cards).name(playerName).build());
        when(gameRepository.getById(gameId)).thenReturn(Game.builder().id(gameId).players(players).build());
        List<TotalAddedValuePlayerDTO> totalAddedValuePlayerDTOList = gameService.getTheListOfPlayers(gameId);
        Assertions.assertEquals(totalAddedValuePlayerDTOList.size(), players.size());
    }

    @Test
    void getCountOfHowManyCardsPerSuitType() {
        Integer gameId = faker.number().randomDigitNotZero();
        Set<Card> cards = new HashSet<>();
        cards.add(Card.builder().face(FaceType.EIGHT).suit(SuitType.CLUBS).build());
        Set<Deck> decks = new HashSet<>();
        Deck deck = Deck.builder().cards(cards).build();
        decks.add(deck);
        when(gameRepository.getById(gameId)).thenReturn(Game.builder().id(gameId).decks(decks).build());
        List<TotalUndealtCardBySuitTypeDTO> totalUndealtCardBySuitTypeDTOList = gameService.getCountOfHowManyCardsPerSuitType(gameId);
        Assertions.assertEquals(1, totalUndealtCardBySuitTypeDTOList.size());
    }

    @Test
    void getCountOfEachCard() {
        Integer gameId = faker.number().randomDigitNotZero();
        Set<Card> cards = new HashSet<>();
        cards.add(Card.builder().face(FaceType.EIGHT).suit(SuitType.CLUBS).build());
        Set<Deck> decks = new HashSet<>();
        Deck deck = Deck.builder().cards(cards).build();
        decks.add(deck);
        when(gameRepository.getById(gameId)).thenReturn(Game.builder().id(gameId).decks(decks).build());
        List<TotalUndealtCardBySuitAndFaceTypeDTO> totalUndealtCardBySuitAndFaceTypeDTOList = gameService.getCountOfHowManyCardsPerSuitAndFaceType(gameId);
        Assertions.assertEquals(1, totalUndealtCardBySuitAndFaceTypeDTOList.size());
    }
}