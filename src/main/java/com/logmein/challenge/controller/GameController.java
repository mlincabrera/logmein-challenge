package com.logmein.challenge.controller;

import com.logmein.challenge.dto.CardDTO;
import com.logmein.challenge.dto.TotalAddedValuePlayerDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitAndFaceTypeDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitTypeDTO;
import com.logmein.challenge.service.GameService;
import com.logmein.challenge.service.IGameService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 *
 */
@RestController
@RequestMapping("/games")
@Getter
public class GameController {

    private final IGameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @PostMapping
    public ResponseEntity<Integer> createGame(@RequestBody @Valid String name) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.createGame(name));
    }

    @DeleteMapping("/{gameId}")
    public ResponseEntity<Void> deleteGame(@PathVariable("gameId") Integer gameId) {
        gameService.deleteGame(gameId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{gameId}/deck/{deckId}")
    public ResponseEntity<Void> addDeckToGame(@PathVariable("gameId") Integer gameId,
                                              @PathVariable("deckId") Integer deckId) {
        gameService.addDeckToGame(gameId, deckId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{gameId}/players/")
    public ResponseEntity<Integer> addPlayerToGame(@PathVariable("gameId") Integer gameId,
                                                   @RequestBody @Valid String playerName) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.addPlayerToGame(gameId, playerName));
    }

    @DeleteMapping("/{gameId}/players/{playerId}")
    public ResponseEntity<Void> removePlayerToGame(@PathVariable("gameId") Integer gameId,
                                                   @PathVariable("playerId") Integer playerId) {
        gameService.removePlayerToGame(gameId, playerId);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{gameId}/players/{playerId}/deal/{amountOfCards}")
    public ResponseEntity<Void> dealCards(@PathVariable("gameId") Integer gameId,
                                          @PathVariable("amountOfCards") Integer amountOfCards,
                                          @PathVariable("playerId") Integer playerId) {
        gameService.dealCards(gameId, amountOfCards, playerId);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{gameId}/players/{playerId}/listOfCardsForaPlayer")
    public ResponseEntity<List<CardDTO>> listOfCardsForaPlayer(@PathVariable("gameId") Integer gameId,
                                                               @PathVariable("playerId") Integer playerId) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.getTheListOfCardsForaPlayer(gameId, playerId));
    }

    @GetMapping("/{gameId}/players/")
    public ResponseEntity<List<TotalAddedValuePlayerDTO>> listOfPlayers(@PathVariable("gameId") Integer gameId) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.getTheListOfPlayers(gameId));
    }

    @GetMapping("/{gameId}/countOfHowManyCardsPerSuitType")
    public ResponseEntity<List<TotalUndealtCardBySuitTypeDTO>> countOfHowManyCardsPerSuitType(@PathVariable("gameId") Integer gameId) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.getCountOfHowManyCardsPerSuitType(gameId));
    }

    @GetMapping("/{gameId}/countOfHowManyCardsPerSuitAndFaceType")
    public ResponseEntity<List<TotalUndealtCardBySuitAndFaceTypeDTO>> countOfHowManyCardsPerSuitAndFaceType(@PathVariable("gameId") Integer gameId) {
        return ResponseEntity.status(HttpStatus.OK).body(gameService.getCountOfHowManyCardsPerSuitAndFaceType(gameId));
    }

    @PostMapping("/{gameId}/shuffle")
    public ResponseEntity<Void> shuffle(@PathVariable("gameId") Integer gameId) {
        gameService.shuffle(gameId);
        return ResponseEntity.noContent().build();
    }

}
