package com.logmein.challenge.controller;

import com.logmein.challenge.service.IDeckService;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 */
@RestController
@RequestMapping("/decks")
@Getter
public class DeckController {

    private final IDeckService deckService;

    public DeckController(IDeckService deckService) {
        this.deckService = deckService;
    }

    @PostMapping
    public ResponseEntity<Integer> createDeck() {
        return ResponseEntity.status(HttpStatus.OK).body(deckService.createDeck().getId());
    }

}
