package com.logmein.challenge.exception;

import lombok.Getter;

@Getter

public enum ErrorCode {
    GENERIC_ERROR(0, "Internal server error"),
    NOT_FOUND(1, "Not found");

    private Integer code;
    private String message;

    ErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
