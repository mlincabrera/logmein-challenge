package com.logmein.challenge.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class CardGameControllerAdvice {

    @ResponseBody
    @ExceptionHandler(CardGameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse cardGameExceptionHandler(CardGameException ex) {
        return ErrorResponse.builder()
                .code(ex.getErrorCode().getCode())
                .message(ex.getErrorCode().getMessage())
                .build();
    }

    @ResponseBody
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ErrorResponse genericExceptionHandler(Exception ex) {
        return ErrorResponse.builder()
                .code(ErrorCode.GENERIC_ERROR.getCode())
                .message(ex.getMessage())
                .build();
    }

}
