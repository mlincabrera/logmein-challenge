package com.logmein.challenge.exception;

import lombok.Getter;

@Getter
public class CardGameException extends RuntimeException {
    private static final long serialVersionUID = 4L;
    private final ErrorCode errorCode;

    public CardGameException(ErrorCode errorCode, Exception e) {
        super(e);
        this.errorCode = errorCode;
    }

    public CardGameException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
