package com.logmein.challenge.util;

import com.logmein.challenge.model.Card;
import com.logmein.challenge.model.FaceType;
import com.logmein.challenge.model.SuitType;
import lombok.experimental.UtilityClass;

import java.util.Iterator;
import java.util.Random;
import java.util.Set;
import java.util.stream.IntStream;

import static java.util.Arrays.stream;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toSet;

@UtilityClass
public class DeckUtil {

    public static final Random RANDOM = new Random();

    public static Set<Card> generateCards() {
        Iterator<Integer> indexes = getIndexIterator(SuitType.values().length * FaceType.values().length);
        return stream(SuitType.values())
                .flatMap(s -> stream(FaceType.values()).map(f -> Card.builder().suit(s).face(f).index(indexes.next()).build()))
                .collect(toSet());
    }

    public static Iterator<Integer> getIndexIterator(Integer size) {
        return IntStream
                .rangeClosed(0, size - 1)
                .boxed()
                .sorted(comparingInt(p -> Math.abs(RANDOM.nextInt(size))))
                .iterator();
    }
}
