package com.logmein.challenge.service;

import com.logmein.challenge.model.Card;
import com.logmein.challenge.model.Deck;
import com.logmein.challenge.respository.DeckRepository;
import com.logmein.challenge.util.DeckUtil;
import org.springframework.stereotype.Service;

import java.util.Iterator;

import static java.util.Comparator.comparingInt;

@Service
public class DeckService implements IDeckService {

    private final DeckRepository deckRepository;


    public DeckService(DeckRepository deckRepository) {
        this.deckRepository = deckRepository;
    }

    @Override
    public Deck save(Deck deck) {
        return deckRepository.save(deck);
    }

    @Override
    public Deck createDeck() {
        Deck deck = Deck.builder()
                .cards(DeckUtil.generateCards())
                .build();
        save(deck);
        return deck;
    }

    @Override
    public Deck getById(Integer deckId) {
        return deckRepository.getById(deckId);
    }

    @Override
    public Card nextCardToDeal(Deck deck) {
        Card card = deck.getCards().stream().min(comparingInt(Card::getIndex)).orElseThrow();
        deck.getCards().removeIf(d -> d.getId().equals(card.getId()));
        return card;
    }

    @Override
    public void shuffle(Deck deck) {
        Iterator<Integer> indexes = DeckUtil.getIndexIterator(deck.getCards().size());
        deck.getCards().forEach(c -> c.setIndex(indexes.next()));
        save(deck);
    }

}
