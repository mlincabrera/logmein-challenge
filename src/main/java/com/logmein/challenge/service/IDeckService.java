package com.logmein.challenge.service;

import com.logmein.challenge.model.Card;
import com.logmein.challenge.model.Deck;

public interface IDeckService {
    Deck save(Deck deck);

    Deck createDeck();

    Deck getById(Integer deckId);

    Card nextCardToDeal(Deck deck);

    void shuffle(Deck deck);
}
