package com.logmein.challenge.service;

import com.logmein.challenge.dto.CardDTO;
import com.logmein.challenge.dto.TotalAddedValuePlayerDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitAndFaceTypeDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitTypeDTO;

import java.util.List;

public interface IGameService {
    Integer createGame(String name);

    void deleteGame(Integer gameId);

    void addDeckToGame(Integer gameId, Integer deckId);

    Integer addPlayerToGame(Integer gameId, String playerName);

    void removePlayerToGame(Integer gameId, Integer playerId);

    void dealCards(Integer gameId, Integer amountOfCards, Integer playerId);

    List<CardDTO> getTheListOfCardsForaPlayer(Integer gameId, Integer playerId);

    List<TotalAddedValuePlayerDTO> getTheListOfPlayers(Integer gameId);

    List<TotalUndealtCardBySuitTypeDTO> getCountOfHowManyCardsPerSuitType(Integer gameId);

    List<TotalUndealtCardBySuitAndFaceTypeDTO> getCountOfHowManyCardsPerSuitAndFaceType(Integer gameId);

    void shuffle(Integer gameId);
}
