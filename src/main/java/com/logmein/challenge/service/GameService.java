package com.logmein.challenge.service;

import com.logmein.challenge.dto.CardDTO;
import com.logmein.challenge.dto.TotalAddedValuePlayerDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitAndFaceTypeDTO;
import com.logmein.challenge.dto.TotalUndealtCardBySuitTypeDTO;
import com.logmein.challenge.exception.CardGameException;
import com.logmein.challenge.exception.ErrorCode;
import com.logmein.challenge.model.*;
import com.logmein.challenge.respository.GameRepository;
import com.logmein.challenge.respository.PlayerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.*;


@Service
@Transactional
@AllArgsConstructor
public class GameService implements IGameService {

    private static final Random RANDOM = new Random();
    private final DeckService deckService;
    private final GameRepository gameRepository;
    private final PlayerRepository playerRepository;

    @Override
    public Integer createGame(String name) {
        return gameRepository.save(Game.builder().name(name).build()).getId();
    }

    @Override
    public void deleteGame(Integer gameId) {
        gameRepository.deleteById(gameId);
    }

    @Override
    public void addDeckToGame(Integer gameId, Integer deckId) {
        Game game = getGame(gameId);
        Deck deck = deckService.getById(deckId);

        game.getDecks().add(deck);
        gameRepository.save(game);
    }

    @Override
    public Integer addPlayerToGame(Integer gameId, String playerName) {
        Game game = getGame(gameId);
        Player player = playerRepository.save(Player.builder().name(playerName).build());
        game.getPlayers().add(player);
        gameRepository.save(game);
        return player.getId();
    }

    @Override
    public void removePlayerToGame(Integer gameId, Integer playerId) {
        Game game = getGame(gameId);
        game.getPlayers().removeIf(player -> player.getId().equals(playerId));
        gameRepository.save(game);
    }

    @Override
    public void dealCards(Integer gameId, Integer amountOfCards, Integer playerId) {
        Game game = getGame(gameId);
        Player player = playerRepository.getById(playerId);
        while (amountOfCards-- > 0) {
            if (!game.getDecks().isEmpty()) {
                int randomDeck = game.getDecks().size() == 1 ? 0 : Math.abs(RANDOM.nextInt((game.getDecks().size() - 1)));
                Deck deck = game.getDecks().stream().skip(randomDeck).findFirst().orElse(null);
                Card card = deckService.nextCardToDeal(deck);
                player.getCards().add(card);

                deckService.save(deck);
                playerRepository.save(player);
            }
        }
    }

    @Override
    public List<CardDTO> getTheListOfCardsForaPlayer(Integer gameId, Integer playerId) {
        Player player = playerRepository.getById(playerId);
        if(!player.getGameId().equals(gameId)){
            throw new CardGameException(ErrorCode.NOT_FOUND);
        }
        return player.getCards().stream()
                .map(c -> CardDTO.builder().suit(c.getSuit()).face(c.getFace()).build())
                .collect(toList());
    }

    @Override
    public List<TotalAddedValuePlayerDTO> getTheListOfPlayers(Integer gameId) {
        Game game = getGame(gameId);
        return game.getPlayers().stream()
                .sorted(comparingInt(Player::getTotalCardValue).reversed())
                .map(p -> TotalAddedValuePlayerDTO.builder().id(p.getId()).name(p.getName()).total(p.getTotalCardValue()).build())
                .distinct().collect(toList());
    }

    @Override
    public List<TotalUndealtCardBySuitTypeDTO> getCountOfHowManyCardsPerSuitType(Integer gameId) {
        Game game = getGame(gameId);
        List<Card> cards = game.getDecks().stream().flatMap(d -> d.getCards().stream()).collect(toList());
        return cards.stream()
                .collect(groupingBy(Card::getSuit, counting()))
                .entrySet().stream()
                .map(e -> TotalUndealtCardBySuitTypeDTO.builder().suit(e.getKey()).total(e.getValue().intValue()).build())
                .sorted(comparing(TotalUndealtCardBySuitTypeDTO::getSuit))
                .collect(toList());
    }


    @Override
    public List<TotalUndealtCardBySuitAndFaceTypeDTO> getCountOfHowManyCardsPerSuitAndFaceType(Integer gameId) {
        Game game = getGame(gameId);
        List<Card> cards = game.getDecks().stream().flatMap(d -> d.getCards().stream()).collect(toList());

        Map<SuitType, Map<FaceType, Long>> totalCardGroupBySuitAndFaceType = cards.stream()
                .collect(groupingBy(Card::getSuit, groupingBy(Card::getFace, counting())));

        List<TotalUndealtCardBySuitAndFaceTypeDTO> list = new ArrayList<>();
        totalCardGroupBySuitAndFaceType.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .forEach(entry -> list.addAll(entry.getValue().entrySet().stream()
                        .map(m -> TotalUndealtCardBySuitAndFaceTypeDTO.builder()
                                .suit(entry.getKey())
                                .face(m.getKey())
                                .total(m.getValue().intValue())
                                .build())
                        .sorted(comparing(TotalUndealtCardBySuitAndFaceTypeDTO::getFace).reversed())
                        .collect(toList())));
        return list;
    }

    @Override
    public void shuffle(Integer gameId) {
        Game game = getGame(gameId);
        game.getDecks().forEach(deckService::shuffle);
    }

    private Game getGame(Integer gameId) {
        try {
            return gameRepository.getById(gameId);
        } catch (EntityNotFoundException e) {
            throw new CardGameException(ErrorCode.NOT_FOUND, e);
        }
    }
}
