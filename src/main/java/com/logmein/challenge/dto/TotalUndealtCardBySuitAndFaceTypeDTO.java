package com.logmein.challenge.dto;

import com.logmein.challenge.model.FaceType;
import com.logmein.challenge.model.SuitType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TotalUndealtCardBySuitAndFaceTypeDTO {
    private SuitType suit;
    private FaceType face;
    private Integer total;
}
