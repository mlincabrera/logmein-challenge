package com.logmein.challenge.dto;

import com.logmein.challenge.model.SuitType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TotalUndealtCardBySuitTypeDTO {
    private SuitType suit;
    private Integer total;
}
