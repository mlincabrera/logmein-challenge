package com.logmein.challenge.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TotalAddedValuePlayerDTO {
    private Integer id;
    private String name;
    private Integer total;
}
