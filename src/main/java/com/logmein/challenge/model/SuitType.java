package com.logmein.challenge.model;

public enum SuitType {
    HEARTS,
    SPADES,
    CLUBS,
    DIAMONDS
}
