package com.logmein.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "card")
public class Card {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private SuitType suit;

    private FaceType face;

    @Column(name = "player_id", insertable = false, updatable = false)
    private Integer playerId;

    @Column(name = "deck_id", insertable = false, updatable = false)
    private Integer deckId;

    private Integer index;

}
