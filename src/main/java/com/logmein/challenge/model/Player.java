package com.logmein.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.FetchType.EAGER;
import static javax.persistence.GenerationType.IDENTITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "player")
public class Player {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    private String name;

    @Builder.Default
    @OneToMany(cascade = ALL, orphanRemoval = true, fetch = EAGER)
    @JoinColumn(name = "player_id")
    private Set<Card> cards = new HashSet<>();

    @Column(name = "game_id", insertable = false, updatable = false)
    private Integer gameId;

    public Integer getTotalCardValue() {
        return cards.stream().mapToInt(c -> c.getFace().getValue()).sum();
    }
}
