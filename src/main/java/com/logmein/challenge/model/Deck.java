package com.logmein.challenge.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.CascadeType.ALL;
import static javax.persistence.GenerationType.IDENTITY;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "deck")
public class Deck {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Integer id;

    @OneToMany(cascade = ALL, orphanRemoval = true)
    @JoinColumn(name = "deck_id")
    @Builder.Default
    private Set<Card> cards = new HashSet<>();


    @Column(name = "game_id", insertable = false, updatable = false)
    private Integer gameId;

}
